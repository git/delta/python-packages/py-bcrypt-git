name: CI
on:
  pull_request: {}
  push:
    branches:
      - main
    tags:
      - '*'

jobs:
  macos:
    runs-on: macos-latest
    strategy:
      matrix:
         PYTHON:
          - {VERSION: "3.6", TOXENV: "py36"}
          - {VERSION: "3.10", TOXENV: "py310"}
    name: "Python ${{ matrix.PYTHON.VERSION }} on macOS"
    steps:
      - uses: actions/checkout@v3.5.2
      - name: Setup python
        id: setup-python
        uses: actions/setup-python@v4.6.0
        with:
          python-version: ${{ matrix.PYTHON.VERSION }}
      - uses: actions/cache@v3.3.1
        timeout-minutes: 5
        with:
          path: |
            ~/Library/Caches/pip/
            ~/.cargo/bin/
            ~/.cargo/registry/index/
            ~/.cargo/registry/cache/
            ~/.cargo/registry/src/
            ~/.cargo/git/db/
            src/_bcrypt/target/
          key: ${{ runner.os }}-${{ steps.setup-python.outputs.python-version }}-cargo-${{ hashFiles('**/Cargo.lock') }}
      - run: pip install tox
      - run: tox
        env:
          TOXENV: ${{ matrix.PYTHON.TOXENV }}
          CARGO_TARGET_DIR: ${{ format('{0}/src/_bcrypt/target/', github.workspace) }}

  windows:
    runs-on: windows-latest
    strategy:
      matrix:
        WINDOWS:
          - {ARCH: 'x86', WINDOWS: 'win32', RUST_TRIPLE: 'i686-pc-windows-msvc'}
          - {ARCH: 'x64', WINDOWS: 'win64', RUST_TRIPLE: 'x86_64-pc-windows-msvc'}
        PYTHON:
          - {VERSION: "3.6", TOXENV: "py36"}
          - {VERSION: "3.10", TOXENV: "py310"}
    name: "Python ${{ matrix.PYTHON.VERSION }} on ${{ matrix.WINDOWS.WINDOWS }}"
    steps:
      - uses: actions/checkout@v3.5.2
      - name: Setup python
        id: setup-python
        uses: actions/setup-python@v4.6.0
        with:
          python-version: ${{ matrix.PYTHON.VERSION }}
          architecture: ${{ matrix.WINDOWS.ARCH }}
      - uses: actions/cache@v3.3.1
        timeout-minutes: 5
        with:
          path: |
            ~/AppData/Local/pip/Cache/
            ~/.cargo/bin/
            ~/.cargo/registry/index/
            ~/.cargo/registry/cache/
            ~/.cargo/registry/src/
            ~/.cargo/git/db/
            src/_bcrypt/target/
          key: ${{ runner.os }}-${{ matrix.WINDOWS.ARCH }}-${{ steps.setup-python.outputs.python-version }}-cargo-${{ hashFiles('**/Cargo.lock') }}      

      - run: pip install tox
      - run: tox
        env:
          TOXENV: ${{ matrix.PYTHON.TOXENV }}
          CARGO_TARGET_DIR: ${{ format('{0}/src/_bcrypt/target/', github.workspace) }}
  linux:
    runs-on: ubuntu-20.04
    strategy:
      matrix:
        PYTHON:
          - {VERSION: "3.9", TOXENV: "pep8,packaging"}
          - {VERSION: "3.9", TOXENV: "mypy"}
          - {VERSION: "3.6", TOXENV: "py36"}
          - {VERSION: "3.7", TOXENV: "py37"}
          - {VERSION: "3.8", TOXENV: "py38"}
          - {VERSION: "3.9", TOXENV: "py39"}
          - {VERSION: "3.10", TOXENV: "py310"}
          - {VERSION: "pypy-3.7-v7.3.9", TOXENV: "pypy3"}
          - {VERSION: "pypy-3.8-v7.3.9", TOXENV: "pypy3"}
          - {VERSION: "pypy-3.9-v7.3.9", TOXENV: "pypy3"}

          # MSRV
          - {VERSION: "3.10", TOXENV: "py310", RUST_VERSION: "1.56.0"}
          - {VERSION: "3.10", TOXENV: "py310", RUST_VERSION: "beta"}
          - {VERSION: "3.10", TOXENV: "py310", RUST_VERSION: "nightly"}
    name: "${{ matrix.PYTHON.TOXENV }} on linux, Rust ${{ matrix.PYTHON.RUST_VERSION || 'stable' }}"
    steps:
      - uses: actions/checkout@v3.5.2
      - name: Setup python
        id: setup-python
        uses: actions/setup-python@v4.6.0
        with:
          python-version: ${{ matrix.PYTHON.VERSION }}
      - uses: actions/cache@v3.3.1
        timeout-minutes: 5
        with:
          path: |
            ~/.cache/pip/
            ~/.cargo/bin/
            ~/.cargo/registry/index/
            ~/.cargo/registry/cache/
            ~/.cargo/registry/src/
            ~/.cargo/git/db/
            src/_bcrypt/target/
          key: ${{ runner.os }}-${{ matrix.PYTHON.VERSION }}-${{ steps.setup-python.outputs.python-version }}-cargo-${{ hashFiles('**/Cargo.lock') }}
      - uses: dtolnay/rust-toolchain@52e69531e6f69a396bc9d1226284493a5db969ff
        with:
          toolchain: ${{ matrix.PYTHON.RUST_VERSION || 'stable' }}

      - run: pip install tox
      - run: tox
        env:
          TOXENV: ${{ matrix.PYTHON.TOXENV }}
          CARGO_TARGET_DIR: ${{ format('{0}/src/_bcrypt/target/', github.workspace) }}

  linux-distros:
    runs-on: ubuntu-latest
    container: ghcr.io/pyca/cryptography-runner-${{ matrix.IMAGE.IMAGE }}
    strategy:
      matrix:
        IMAGE:
          - {IMAGE: "alpine", TOXENV: "py311"}
    name: "${{ matrix.IMAGE.TOXENV }} on ${{ matrix.IMAGE.IMAGE }}"
    steps:
      - uses: actions/checkout@v3.5.2
        with:
          persist-credentials: false
      - uses: actions/cache@v3.3.1
        timeout-minutes: 5
        with:
          path: |
            ~/.cache/pip/
            ~/.cargo/bin/
            ~/.cargo/registry/index/
            ~/.cargo/registry/cache/
            ~/.cargo/registry/src/
            ~/.cargo/git/db/
            src/_bcrypt/target/
          key: ${{ runner.os }}-${{ matrix.IMAGE.IMAGE }}-cargo-${{ hashFiles('**/Cargo.lock') }}
      - run: /venv/bin/pip install tox
      - run: '/venv/bin/tox'
        env:
          TOXENV: ${{ matrix.IMAGE.TOXENV }}
          RUSTUP_HOME: /root/.rustup
          CARGO_TARGET_DIR: ${{ format('{0}/src/_bcrypt/target/', github.workspace) }}
